#include <iostream>
#include <cstdlib>
#include "src/TEA.h"

int main(int argc, char** argv) {

    if (argc < 3) {
        std::cerr << "Can not find key and file path" << std::endl;
        std::abort();
    }

    std::string INA_flag = argv[1]; //����祭�� ���� �� ��ࠬ��஢
    std::string INA_filename = argv[2]; //����祭�� ��� � 䠩�� ��� ��஢���� �� ��ࠬ��஢

    if (INA_flag.empty()) {
        std::cerr << "Flag -e or -d was not found" << std::endl;
        std::abort();
    }

    if (INA_filename.empty()) {
        std::cerr << "File path was not found" << std::endl;
        std::abort();
    }

    auto* INA_tea = new TEA(); //ᮧ����� ��ꥪ� ����� 訢�騪�/����஢騪�

    if (INA_flag == "-d") {
        if (argv[3] != nullptr) {
            std::cout << argv[3] << std::endl;
            INA_tea->INA_runDecryption(INA_filename, argv[3]);
        } else {
            INA_tea->INA_runDecryption(INA_filename);
        }
    }

    if (INA_flag == "-e") {
        INA_tea->INA_runEncryption(INA_filename);
    }

    return 0;
}
