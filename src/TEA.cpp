//
// Created by Nail on 10.02.2021.
//

#include <cstdlib>
#include <string>
#include <iostream>
#include <ctime>
#include "TEA.h"
#include "FileWorker.h"

/* Вызов шифрования при отсутсвии пути к файлу с ключом */
void TEA::INA_runDecryption(const std::string& INA_filePath) {
    INA_runDecryption(INA_filePath, "key.txt");
}

/* Запуск дешифровки файла */
void TEA::INA_runDecryption(const std::string& INA_filePath, const std::string& INA_keyFilePath) {
    auto* INA_dataFileWorker = new FileWorker(INA_filePath); // Утилита для файла с данными
    auto* INA_keyFileWorker = new FileWorker(INA_keyFilePath); // Утилита для файла с ключом

    values = INA_dataFileWorker->INA_readDataFromFile(2); //Данные конвертированные в uint32
    key = INA_keyFileWorker->INA_readDataFromFile(4);

    INA_decrypt();

    INA_dataFileWorker->INA_setDecryptedFilePath(INA_dataFileWorker->INA_getFilePath());
    INA_dataFileWorker->INA_writeDataToFile(values, 2);

    std::cout << "Decryption finished" << std::endl;
}

/* Дешифровка данных */
void TEA::INA_decrypt() {
    int i = 32; //Количество итераций
    uint32_t delta = 0x9e3779b9; //Дельта δ = (sqrt(5) — 1) * 2^31 = 2654435769 = 9E3779B9
    uint32_t sum = 0xC6EF3720; //Сумма дельт

    while (i-->0) {
        values[1] -= (( values[0] << 4 ) + key[2] ) ^ (values[0] + sum ) ^ (( values[0] >> 5 ) + key[3] );
        values[0] -= (( values[1] << 4 ) + key[0] ) ^ (values[1] + sum ) ^ (( values[1] >> 5 ) + key[1] );
        sum -= delta;
    }
}

/* Запуск шифрования файла */
void TEA::INA_runEncryption(const std::string& INA_filePath) {
    auto* INA_fileWorker = new FileWorker(INA_filePath); // Утилита для файла с данными
    values = INA_fileWorker->INA_readDataFromFile(2); //Данные конвертированные в uint32

    INA_generateKey();
    INA_encrypt();

    auto* INA_encryptedFileWorker = new FileWorker();
    INA_encryptedFileWorker->INA_setEncryptedFilePath(INA_fileWorker->INA_getFilePath());
    INA_encryptedFileWorker->INA_writeDataToFile(values, 2);

    std::cout << "Encryption finished" << std::endl;
}

/* Шифрование данных */
void TEA::INA_encrypt() {
    int i = 32; //Количество итераций
    uint32_t delta = 0x9e3779b9; //Дельта δ = (sqrt(5) — 1) * 2^31 = 2654435769 = 9E3779B9
    uint32_t sum = 0; //Сумма дельт

    while (i-->0) {
        sum += delta;
        values[0] += (( values[1] << 4 ) + key[0] ) ^ (values[1] + sum ) ^ (( values[1] >> 5 ) + key[1] );
        values[1] += (( values[0] << 4 ) + key[2] ) ^ (values[0] + sum ) ^ (( values[0] >> 5 ) + key[3] );
    }
}

/* Генерация ключа */
void TEA::INA_generateKey() {
    srand(static_cast<unsigned int>(time(nullptr)));

    for (int i = 0; i < 4; i++) {
        this->key[i] = rand() << 16 | rand();
    }

    auto* INA_fileWorker = new FileWorker("key.txt"); //Утилита для работы с файлами для ключа
    INA_fileWorker->INA_writeDataToFile(this->key, 4);
}