//
// Created by Nail on 10.02.2021.
//

#ifndef IS_1_FILEWORKER_H
#define IS_1_FILEWORKER_H


#include <cstdint>

class FileWorker {
private:
    std::string INA_filePath;
    std::string INA_getNewFileName(std::string INA_fileName, int INA_index);
    static bool INA_isFileExists(const std::string& INA_fileName);
public:
    FileWorker();
    explicit FileWorker(std::string filePath);
    void INA_writeDataToFile(uint32_t* data, int size);
    uint32_t* INA_readDataFromFile(int INA_resultSize);
    void INA_setEncryptedFilePath(const std::string& INA_decryptedFilePath);
    void INA_setDecryptedFilePath(std::string INA_encryptedFilePath);

    std::string INA_getFilePath();
};


#endif //IS_1_FILEWORKER_H
