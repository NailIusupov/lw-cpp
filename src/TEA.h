//
// Created by Nail on 10.02.2021.
//

#ifndef IS_1_TEA_H
#define IS_1_TEA_H


#include <cstdint>

class TEA {
private:
    uint32_t* key = new uint32_t[4];
    uint32_t* values = new uint32_t[2];
    void INA_generateKey();
    void INA_encrypt();
    void INA_decrypt();
    static char getRandomChar();
public:
    void INA_runEncryption(const std::string& INA_filePath);
    void INA_runDecryption(const std::string& INA_filePath);
    void INA_runDecryption(const std::string& INA_filePath, const std::string& INA_keyFilePath);
};


#endif //IS_1_TEA_H
