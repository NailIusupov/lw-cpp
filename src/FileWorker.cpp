//
// Created by Nail on 10.02.2021.
//

#include <fstream>
#include <iostream>
#include <vector>
#include <bitset>
#include "FileWorker.h"

FileWorker::FileWorker() = default;

FileWorker::FileWorker(std::string filePath) {
    this->INA_filePath = std::move(filePath);
}

/* Функция записи uint32 данных в файл */
void FileWorker::INA_writeDataToFile(uint32_t *data, int size) {
    auto INA_fileName = INA_getNewFileName(this->INA_filePath, 0); //Получение имени для создания нового файла

    std::ofstream INA_file; //Файл для записи
    INA_file.open(INA_fileName, std::ios::binary);

    for (int i = 0; i < size; i++) {
        std::bitset<32> bits(data[i]);
        INA_file.write(reinterpret_cast<char*>(&bits), 4);
    }
}

/* Чтение массива символов из файла */
uint32_t* FileWorker::INA_readDataFromFile(int INA_resultSize) {
    if (!INA_isFileExists(this->INA_filePath)) {
        std::cerr << "File " << this->INA_filePath << " was not found" << std::endl;
        std::abort();
    }

    std::ifstream INA_file; //Файл, для чтения
    INA_file.open(this->INA_filePath, std::ios_base::binary);

    auto* INA_result = new uint32_t[INA_resultSize]; //Массив с результатом чтения

    for(int i = 0; i < INA_resultSize; i++) {
        INA_file.read((char*)&INA_result[i], sizeof(INA_result[i]));
    }

    return INA_result;
}

/* Геттер для приватного поля*/
std::string FileWorker::INA_getFilePath() {
    return this->INA_filePath;
}

/* Добавление .enc для зашифрованного файла */
void FileWorker::INA_setEncryptedFilePath(const std::string& INA_decryptedFilePath) {
    this->INA_filePath = INA_decryptedFilePath + ".enc";
}

/* удаление .enc для расшифрованного файла */
void FileWorker::INA_setDecryptedFilePath(std::string INA_encryptedFilePath) {
    INA_encryptedFilePath.erase(INA_encryptedFilePath.find(".enc"), 4);
    this->INA_filePath = INA_encryptedFilePath;
}

/* Проверка на существование файла */
bool FileWorker::INA_isFileExists(const std::string& INA_fileName) {
    std::ifstream f(INA_fileName);
    return f.good();
}

/* Получение имени для нового файла */
std::string FileWorker::INA_getNewFileName(std::string INA_fileName, int INA_index) {
    if (!INA_isFileExists(INA_fileName) || INA_fileName.find(".enc") != -1 || INA_fileName.find("key") != -1) {
        return INA_fileName;
    }

    if (INA_index == 0) {
        INA_fileName.replace(INA_fileName.find(".txt"), 4, "(1).txt");
        return INA_getNewFileName(INA_fileName, INA_index + 1);
    }

    INA_fileName.replace(INA_fileName.find(std::to_string(INA_index )), 1, std::to_string(INA_index + 1 ));
    return INA_getNewFileName(INA_fileName, INA_index + 1);
}
